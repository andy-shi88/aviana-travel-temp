# APIARY Blueprint for Travel API

## Overview
This repository will store the basic api blueprints that will be built for the travel API.
We use apiary for the blueprint, in which we will also have the documentation of the API in.


## Setup
- Install Hercule by `sudo npm install hercule -g` to install hercule globally. - this is the tool for combining all the apiary subblueprint to one file.

## Compile
- run `hercule blueprints/blueprint.apib -o apiary.apib` you'll get the compiled result in `apiary.apib` you can then copy and paste to the apiary dashboard.

## Adding Blueprints

- for data structure add to `blueprint/data/` following the resources name
- for blueprint add to `blueprint/` following the resources name.
- after adding all those, include them into `blueprint/blueprint.apib` before compiling.

